/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

/**
 *
 * @author Alicia
 */
public class Coordenadas {
    private final int x;
    private final int y;
    public Coordenadas(int x, int y){
        this.x = x;
        this.y = y;
    }
    /**
     * Constructor copia
     * @param c 
     */
    public Coordenadas(Coordenadas c){
        this.x = c.x;
        this.y = c.y;
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }
    public int distancia(Coordenadas b){
        int dx = b.getX()-x;
        int dy = b.getY()-y;
        return (int) Math.sqrt(dy*dy+dx*dx);
    }
}
