/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import IA.Desastres.Grupos;

/**
 *
 * @author Alicia
 */
public class Salida {

    private int[] gruposRecogidos; /* Indice del grupo en el arrayList de grupos de AI_Desastres */

    private int numGrupos;
    private int tiempo;
    private int numGruposPrioridad; // Numero de grupos de la salida que tienen prioridad 1    

    // Constructors
    public Salida(int[] gruposRecogidos, int numGrupos, int tiempo, int numGruposPrioridad) {
        this.gruposRecogidos = gruposRecogidos;
        this.numGrupos = numGrupos;
        this.tiempo = tiempo;
        this.numGruposPrioridad = numGruposPrioridad;
    }

    /**
     * Constructor copia
     *
     * @param s
     */
    public Salida(Salida s) {
        this.numGrupos = s.numGrupos;
        this.gruposRecogidos = new int[s.gruposRecogidos.length];
        System.arraycopy(s.gruposRecogidos, 0, this.gruposRecogidos, 0, s.gruposRecogidos.length);
        this.tiempo = s.tiempo;
        this.numGruposPrioridad = s.numGruposPrioridad;
    }

    public Salida() {
        gruposRecogidos = new int[3];
        numGrupos = 0;
        tiempo = 0;
        numGruposPrioridad = 0;
    }

    // Getters and Setters
    public int[] getGruposRecogidos() {
        return gruposRecogidos;
    }

    public int getidGrupo(int index) {
        if (index < 0 || index >= numGrupos) {
            throw new IndexOutOfBoundsException();
        }
        return gruposRecogidos[index];
    }

    public void setGruposRecogidos(int[] gruposRecogidos) {
        this.gruposRecogidos = gruposRecogidos;
    }

    public double getTiempo() {
        return tiempo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

    public int getNumGruposPrioridad() {
        return numGruposPrioridad;
    }

    public void setNumGruposPrioridad(int prioridad) {
        this.numGruposPrioridad = prioridad;
    }

    public int getNumGrupos() {
        return numGrupos;
    }

    public void setNumGrupos(int numGrupos) {
        this.numGrupos = numGrupos;
    }

    // Other methods
    public boolean adicionarGrupo(int idGrupo, int orden, Grupos listaGrupos) { // orden debe ir de 1 a 3
        int i;
        if (numGrupos >= 3) {
            //System.out.println("La salida ya tiene 3 grupos");
            return false;
        }
        if (orden >= numGrupos) {
            if (!numMaximoPersonas(listaGrupos, idGrupo)) {
                gruposRecogidos[numGrupos] = idGrupo;
                numGrupos++;
                if(listaGrupos.get(idGrupo).getPrioridad() == 1){
                    numGruposPrioridad++;
                }
                //System.out.println("Orden de salida del grupo adicionado: " + numGrupos );
                return true;
            }
        } else {
            if (!numMaximoPersonas(listaGrupos, idGrupo)) {
                gruposRecogidos[2] = gruposRecogidos[1];
                if (orden == 0) {
                    gruposRecogidos[1] = gruposRecogidos[0];
                }
                gruposRecogidos[orden] = idGrupo;
                if(listaGrupos.get(idGrupo).getPrioridad() == 1){
                    numGruposPrioridad++;
                }
                numGrupos++;
                return true;
            }
        }
        //System.out.println("Capacidad del helicoptero explotada, no se puede adicionar el grupo.");
        return false;
    }

    private boolean numMaximoPersonas(Grupos listaGrupos, int idNuevoGrupo) {
        int numPersonas = 0;
        numPersonas = numPersonas(listaGrupos) + listaGrupos.get(idNuevoGrupo).getNPersonas();
        if (numPersonas > 15) {
            return true;
        } else {
            return false;
        }
    }

    public int numPersonas(Grupos listaGrupos) {
        int i, numPersonas;
        numPersonas = 0;

        for (i = 0; i < numGrupos; i++) {
            numPersonas += listaGrupos.get(gruposRecogidos[i]).getNPersonas();
        }
        return numPersonas;
    }

    /**
     *
     * @param orden
     * @return null si no hay un grupo en la posicion orden, el id del grupo en
     * la posicion orden en caso contrario
     */
    public Integer eliminarGrupo(int orden, Grupos listaGrupos) {
        //System.out.println("eliminargrupo");
        if (orden < numGrupos) {
            Integer x = gruposRecogidos[orden];
            for (int i = orden + 1; i < numGrupos; ++i) {
                gruposRecogidos[i - 1] = gruposRecogidos[i];
            }
            --numGrupos;
            if(listaGrupos.get(x).getPrioridad()==1){
                --numGruposPrioridad;
            }
            return x;
        }
        return null;
    }

    public String printGrupo(Grupos g) {
        //System.out.println("printgrupo");
        String s = new String("");
        try {
            for (int i = 0; i < numGrupos - 1; ++i) {
                int x = gruposRecogidos[i];
                s = s + "grupo " + x + "(" + g.get(x).getNPersonas() + " personas), ";

            }
            s = s + "grupo " + gruposRecogidos[numGrupos - 1] + "(" + g.get(gruposRecogidos[numGrupos - 1]).getNPersonas() + " personas)";
            s = s + "\n";
        } catch (Exception e) {
            System.out.println("error en printgrupo");
            e.printStackTrace();
        }
        return s;
    }

    public boolean sustituirGrupo(int idGrupo, int orden, Grupos listaGrupos) {

        if (orden < gruposRecogidos.length) {
            gruposRecogidos[orden] = idGrupo;
            //System.out.println("Orden de salida del grupo adicionado: " + numGrupos );
            return true;
        }
        return false;

    }
}
