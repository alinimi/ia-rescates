/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estat;

import aima.search.framework.HeuristicFunction;

/**
 *
 * @author Alicia
 */
public class HeuristicFunctionTotalTime implements HeuristicFunction {

    /**
     * Obtiene la suma de los tiempos totales de rescate de los helicópteros
     * en minutos
     * @param o instancia de EstadoRescates
     * @return Suma de los tiempos de rescate de los helicópteros
     */
    @Override
    public double getHeuristicValue(Object o) {
        EstadoRescates estat = (EstadoRescates) o;
        return estat.getTiempoTotal();
    }

}
