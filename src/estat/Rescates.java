/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estat;

import aima.search.framework.Problem;
import aima.search.framework.Search;
import aima.search.framework.SearchAgent;
import aima.search.informed.HillClimbingSearch;
import aima.search.informed.SimulatedAnnealingSearch;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

/**
 *
 * @author Alicia
 */
public class Rescates {

    /**
     * Método main del proyecto
     * @param args 
     */
    public static void main(String[] args) {

        System.out.println("total,ejecucion");
        for(int i = 1; i < 10; i+=1)
        rescatesHillClimbing(100, 5, i, 0);

    }

    /**
     * ejecuta una búsqueda local con el método de hill climbing
     */
    private static void rescatesHillClimbing(int g, int c, int h, double p) {
        try {
            long startTime = System.currentTimeMillis();
            int seed = (int) startTime;
            //System.out.println("Rescates con hill climbing");
            //g,c,h,s
            Problem problem = new Problem(new EstadoRescates(g, c, h, seed),
                    new RescatesSuccessorFunction(),
                    new RescatesGoalTest(), new HeuristicFunctionPriorityTime(p));
            EstadoRescates e = (EstadoRescates) problem.getInitialState();
            //System.out.println(AI_Desastres.grupos_toString(e.getGrupos()));
            //System.out.println(AI_Desastres.centros_toString(e.getCentros()));
            
            Search search = new HillClimbingSearch();
            //System.out.println("asdfasdf");
            SearchAgent agent = new SearchAgent(problem, search);
            //printActions(agent.getActions());
            //printInstrumentation(agent.getInstrumentation());
            List<EstadoRescates> l = search.getPathStates();
            EstadoRescates estadofinal = l.get(l.size() - 1);
            System.out.print(estadofinal.getTiempoTotal());
            System.out.print(","+estadofinal.getTiempoPrioritario());
            long endTime = System.currentTimeMillis();

            System.out.print(","+(endTime - startTime));
            System.out.println("");
        } catch (Exception e) {

        }
    }

    /**
     * Ejecuta una búsqueda local con el método de simulated annealing
     * @param iter Número de iteraciones totales de simulated annealing
     * @param s Número de iteraciones de simulated annealing por temperatura
     * @param k Parámetro de la función de aceptación de estados de simulated
     * annealing
     * @param lam Parámetro de la función de aceptación de estados de simulated
     * annealing
     */
    private static void rescatesSimulatedAnnealing(int g, int c, int h, double p, int iter, int s, int k, double lam) {
        try {
            long startTime = System.currentTimeMillis();
            int seed = (int) startTime;
            Problem problem = new Problem(new EstadoRescates(g, c, h, seed),
                    new RescatesSuccessorFunction(),
                    new RescatesGoalTest(), new HeuristicFunctionPriorityTime(p));
            EstadoRescates e = (EstadoRescates) problem.getInitialState();
            //System.out.print(AI_Desastres.grupos_toString(e.getGrupos()));
            //System.out.print(AI_Desastres.centros_toString(e.getCentros()));
            
            //System.out.print(e.printOrden());
            //System.out.print("findelprintorden");
            Search search = new SimulatedAnnealingSearch(iter, s, k, lam);
            SearchAgent agent = new SearchAgent(problem, search);

            //printActions(agent.getActions());

            //printInstrumentation(agent.getInstrumentation());
            List<EstadoRescates> l = search.getPathStates();
            long endTime = System.currentTimeMillis();
            EstadoRescates estadofinal = (EstadoRescates)search.getGoalState();
               System.out.print(estadofinal.getTiempoTotal());

            System.out.print(","+(endTime - startTime));
            System.out.println("");
            


        } catch (Exception e) {

        }
    }

     private static void rescatesSimulatedAnnealing2() {
        try {
            long startTime = System.currentTimeMillis();
            Problem problem = new Problem(new EstadoRescates(100, 5, 1, 1234),
                    new RescatesSuccessorFunction(),
                    new RescatesGoalTest(), new HeuristicFunctionTotalTime());
            EstadoRescates e = (EstadoRescates) problem.getInitialState();
            System.out.print(AI_Desastres.grupos_toString(e.getGrupos()));
            System.out.print(AI_Desastres.centros_toString(e.getCentros()));
            
            //System.out.print(e.printOrden());
            //System.out.print("findelprintorden");
            Search search = new SimulatedAnnealingSearch(10000, 10, 1, 1);
            SearchAgent agent = new SearchAgent(problem, search);

            printActions(agent.getActions());

            printInstrumentation(agent.getInstrumentation());
            List<EstadoRescates> l = search.getPathStates();
            EstadoRescates esf = (EstadoRescates)search.getGoalState();
            System.out.println("el tiempo total de rescate es: "+esf.getTiempoTotal()+" minutos");
            
            long endTime = System.currentTimeMillis();

            System.out.println("That took " + (endTime - startTime) + " milliseconds");
        } catch (Exception e) {

        }
    }

     /**
      * Muestra por pantalla los estados por los que pasa el problema
      * @param actions lista de estados
      */
    private static void printActions(List actions) {
        try {
            System.out.println("actions size " + actions.size());
            for (int i = 0; i < actions.size(); i++) {
                String action = actions.get(i).toString();
                System.out.println(action);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println(e.getStackTrace());
        }
        System.out.println("fin inst");
    }

    private static void printInstrumentation(Properties properties) {
        System.out.println("instrumentation");
        Iterator keys = properties.keySet().iterator();
        while (keys.hasNext()) {
            String key = (String) keys.next();
            String property = properties.getProperty(key);
            System.out.println(key + " : " + property);
        }
    }
}
