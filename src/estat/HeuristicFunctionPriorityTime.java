/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package estat;

import aima.search.framework.HeuristicFunction;

/**
 *
 * @author Alicia
 */
public class HeuristicFunctionPriorityTime implements HeuristicFunction{
    double p;
    public HeuristicFunctionPriorityTime(double d){
        p = d;
    }
    /**
     * Obtiene la suma de los tiempos totales de rescate de los helicópteros
     * en minutos más la suma de los tiempos de rescate de heridos de los
     * helicópteros
     * @param state instancia de EstadoRescates
     * @return Suma de los tiempos de rescate de los helicópteros
     */
    @Override
    public double getHeuristicValue(Object state) {
        EstadoRescates estat = (EstadoRescates) state;
        return estat.getTiempoTotal()+p*(double)estat.getTiempoPrioritario();
    }
    
}
