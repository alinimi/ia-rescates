/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estat;

import IA.Desastres.Centro;
import IA.Desastres.Centros;
import IA.Desastres.Grupo;
import IA.Desastres.Grupos;
import java.util.Random;
import util.Coordenadas;
import util.Salida;

/**
 *
 * @author Alicia
 */
public class EstadoRescates {

    /**
     * Instancia del problema a resolver
     */
    private static AI_Desastres prob;
    
    /**
     * Array del recorrido de cada helicóptero
     */
    private Helicoptero[] asignacion;
    
    /**
     * Suma de los tiempos de los recorridos de los helicópteros
     */
    private int tiempoTotal;
    
    /**
     * Suma de los tiempos en que los helicópteros han acabado de rescatar a los
     * heridos
     */
    private int tiempoPrioritario;

    
    /**
     * Constructor de EstadoRescates.
     * @param g Número de grupos que hay que rescatar
     * @param c Número de centros en el mapa
     * @param h Número de helicópteros en cada centro
     * @param s Seed para la generación aleatoria de centros y grupos
     */
    public EstadoRescates(int g, int c, int h, int s) {
        prob = new AI_Desastres(g, c, h, s);
        //System.out.println(AI_Desastres.centros_toString(prob.getCentros()));
        //System.out.println(AI_Desastres.grupos_toString(prob.GetGrupos()));
        asignacion = new Helicoptero[h * c];
        for (int i = 0; i < c; ++i) {
            for (int j = 0; j < h; ++j) {
                Centros cen = prob.getCentros();
                Centro centro = cen.get(i);
                int x = centro.getCoordX();

                int y = prob.getCentros().get(i).getCoordY();
                asignacion[h * i + j] = new Helicoptero(x, y);
            }

        }
        solucionInicial2();
        //printOrden();

        tiempoTotal = calculaTiempoTotal();
        tiempoPrioritario = calculaTiempoPrioritario();
    }

    //////no
    /**
     * Getter de grupos.
     * @return Grupos
     */
    public Grupos getGrupos(){
        return prob.GetGrupos();
    }
    
    
    public Centros getCentros(){
        return prob.getCentros();
    }
    public AI_Desastres getProb(){
        return prob;
    }
    //////
    
    /**
     * Constructor copia.
     * @param e Instancia de EstadoRescates
     */
    public EstadoRescates(EstadoRescates e) {
        this.tiempoTotal = e.tiempoTotal;
        this.tiempoPrioritario = e.tiempoPrioritario;
        this.asignacion = new Helicoptero[e.asignacion.length];
        for (int i = 0; i < e.asignacion.length; ++i) {
            this.asignacion[i] = new Helicoptero(e.asignacion[i]);
        }
    }

    /**
     * Getter de la suma del número de helicópteros en los centros.
     * @return Suma del número de helicópteros en los centros
     */
    public int getNumH() {
        return asignacion.length;
    }

    /**
     * Getter del número de salidas de un helicóptero.
     * @param i Índice del helicóptero. Valor en [0,Número de centros*Número
     * de helicópteros por centro)
     * @return 
     */
    public int getNumSalidas(int i) {
        return asignacion[i].size();
    }

    /**
     * Getter de una salida de un helicóptero.
     * @param i Índice del helicóptero. Valor en [0,Número de centros*Número
     * de helicópteros por centro)
     * @param s Índice de la salida del helicóptero
     * @return 
     */
    public Salida getSalida(int i, int s) {
        return asignacion[i].getSalida(s);
    }

    //sols iniciales
    
    /**
     * Método que genera una solucion inicial del problema asignando cada grupo a un
     * helicóptero elegido aleatoriamente y añadiéndolo a su última salida o a
     * una nueva salida que se hará después de la última salida anterior.
     */
    private void solucionInicial1() {
        Random r = new Random();
        for (int i = 0; i < prob.GetGrupos().size(); ++i) {
            Grupo g = prob.GetGrupos().get(i);
            Centros c = prob.getCentros();

            int randHeli = r.nextInt(c.get(0).getNHelicopteros() * c.size());
            asignacion[randHeli].anadirAlFinal(i, prob);
            if (g.getPrioridad() == 1) {
                if (tiempoPrioritario < asignacion[randHeli].getTiempoHeridos()) {
                    tiempoPrioritario = asignacion[randHeli].getTiempoHeridos();
                }
            }
            if (tiempoTotal < asignacion[randHeli].getTiempoTotal()) {
                tiempoTotal = asignacion[randHeli].getTiempoTotal();
            }
        }

    }

    /**
     * Método que genera una solución inicial al problema asignando cada grupo a
     * un helicóptero aleatorio del centro más cercano al grupo, de forma que el
     * número máximo de grupos en una salida sea dos.
     */
    private void solucionInicial2() {
        Random r = new Random();
        for (int i = 0; i < prob.GetGrupos().size(); ++i) {
            Grupo g = prob.GetGrupos().get(i);
            Coordenadas c = new Coordenadas(g.getCoordX(), g.getCoordY());

            Centro cen = prob.getCentros().get(0);
            Coordenadas coordCentro = new Coordenadas(cen.getCoordX(), cen.getCoordY());
            int dist = c.distancia(coordCentro);
            int k = 0;
            //System.out.println("La cosa "+c.distancia(coordCentro));

            for (int j = 1; j < prob.getCentros().size(); ++j) {
                cen = prob.getCentros().get(j);
                coordCentro = new Coordenadas(cen.getCoordX(), cen.getCoordY());
                //System.out.println("La cosa "+c.distancia(coordCentro));
                if (c.distancia(coordCentro) < dist) {
                    dist = c.distancia(coordCentro);
                    k = j;
                }
            }
            int randHeli = r.nextInt(prob.getNHelicopteros());
            //System.out.println("we are having a problem "+(k*prob.getNHelicopteros()+randHeli));
            asignacion[k * prob.getNHelicopteros() + randHeli].anadirMaxDos(i, prob);
            //printOrden();
        }

        calculaTiempoTotal();
        calculaTiempoPrioritario();
        //System.out.println("tiempo prioritario inicial: "+tiempoPrioritario);
    }

    //sucesores
    /**
     * Intercambia las posiciones de dos grupos en el estado
     * @param h1 Índice del helicóptero donde está el grupo 1 
     * @param s1 Índice de la salida del helicóptero h1 donde está el grupo 1
     * @param i1 Orden de la salida s1 del helicóptero h1 que ocupa el grupo 1
     * @param h2 Índice del helicóptero donde está el grupo 2
     * @param s2 Índice de la salida del helicóptero h2 donde está el grupo 2
     * @param i2 Orden de la salida s1 del helicóptero h1 que ocupa el grupo 1
     * @return true si se han intercambiado los grupos, false en caso contrario
     */
    public boolean intercambiarGrupo(int h1, int s1, int i1, int h2, int s2, int i2) {
        int id1, id2;
        try {
            id1 = asignacion[h1].getidGrupo(s1, i1);
            id2 = asignacion[h2].getidGrupo(s2, i2);
        } catch (IndexOutOfBoundsException e) {
            return false;
        }

        Grupos grup = prob.GetGrupos();
        int np1 = asignacion[h1].getSalida(s1).numPersonas(prob.GetGrupos());
        int np2 = asignacion[h2].getSalida(s2).numPersonas(prob.GetGrupos());

        if ((h1 == h2 && s1 == s2) || (np1 - grup.get(id1).getNPersonas() + grup.get(id2).getNPersonas() <= 15
                && np2 - grup.get(id2).getNPersonas() + grup.get(id1).getNPersonas() <= 15)) {

            asignacion[h1].sustituirGrupo(id2, s1, i1, prob);

            asignacion[h2].sustituirGrupo(id1, s2, i2, prob);
            actualizarTiempo(h1,h2);
            return true;

        }

        return false;
    }

    /**
     * Intercambia las posiciones de las salidas i1 e i2 del helicóptero h
     * @param h Índice del helicóptero en la asignación. Valor en [0,numero de
     * centros*número de helicópteros por centro)
     * @param i1 Índice de una salida del helicóptero h. Valor en [0,número de
     * salidas del helicóptero h)
     * @param i2 Índice de una salida del helicóptero h. Valor en [0,número de
     * salidas del helicóptero h)
     */
    public void cambiarOrdenRescate(int h, int i1, int i2) {
        asignacion[h].cambiarOrden(i1, i2, prob);
        calculaTiempoTotal();
        calculaTiempoPrioritario();
    }

    
    /**
     * Elimina un grupo de su posición y lo inserta en otra posición dada, solo
     * si la nueva salida tiene capacidad suficiente para el grupo.
     * @param h1 Índice del helicóptero donde se encuentra el grupo. Valor en [0,numero de
     * centros*número de helicópteros por centro)
     * @param s1 Índice de la salida del helicóptero h1 donde se encuentra el 
     * grupo. Valor en [0,número de salidas de h1)
     * @param i1 Orden que ocupa el grupo en la salida s1 del helicóptero h1.
     * @param h2 Índice del helicóptero donde se quiere colocar el grupo.
     * @param s2 Índice de la salida del helicóptero h2 donde se quiere colocar
     * el grupo
     * @param i2 Orden que se quiere que ocupe el grupo en la salida s2 del 
     * helicóptero h2
     * @return 
     */
    public boolean cambiarUnGrupo(int h1, int s1, int i1, int h2, int s2, int i2) {

            Integer x = asignacion[h1].getSalida(s1).getidGrupo(i1);
            boolean b = asignacion[h2].anadirGrupo(x, s2, i2, prob);
            if(b){
                asignacion[h1].eliminarGrupo(s1, i1, prob);
                actualizarTiempo(h1,h2);
            }
        return b;
    }
    
    /**
     * Actualiza la suma del tiempo de las salidas de los helicópteros h1 y h2 y 
     * calcula el tiempo total del estado.
     * @param h1 Índice de un helicóptero de la asignación
     * @param h2 Índice de un helicóptero de la asignación
     */
    private void actualizarTiempo(int h1,int h2){
        if(h1 == h2){
            asignacion[h2].actualizarTiempo();
        }
        else{
            asignacion[h1].actualizarTiempo();
            asignacion[h1].actualizarTiempo();
        }
        calculaTiempoTotal();
        calculaTiempoPrioritario();
    }

    /**
     * Recorre la asignación y suma el tiempo total de los helicópteros
     * @return suma del tiempo que tardan los helicópteros en rescatar a todos
     * los grupos
     */
    private int calculaTiempoTotal() {
        int tiempo;
        tiempo = 0;
        for (Helicoptero h : asignacion) {
            tiempo += h.getTiempoTotal();
        }
        tiempoTotal = tiempo;
        return tiempo;
    }

    /**
     * Getter del tiempo total de la asignación
     * @return tiempo total de la asignación
     */
    public int getTiempoTotal() {
        return tiempoTotal;
    }

    /**
     * Recorre la asignación y suma el tiempo que tardan los helicópteros en
     * rescatar a su último grupo de prioridad 1 asignado
     * @return suma del tiempo que tardan los helicópteros en rescatar a los
     * grupos de prioridad 1
     */
    private int calculaTiempoPrioritario() {
        int tiempo;
        tiempo = 0;
        for (Helicoptero h : asignacion) {
            tiempo += h.getTiempoHeridos();
        }
        tiempoPrioritario = tiempo;
        return tiempo;
    }

    /**
     * Getter del tiempo que se tarda en reoger a todos los heridos
     * @return tiempo prioritario
     */
    public int getTiempoPrioritario(){
        return tiempoPrioritario;
    }
    /**
     * Intenta añadir el grupo con id pasado como parámetro en la posición
     * indicada
     * @param idGrupo
     * @param h
     * @param s
     * @param i
     * @return True si se ha añadido el grupo en la posición dada, false si
     * no se puede añadir
     */
    public boolean anadirGrupo(int idGrupo, int h, int s, int i) {
        boolean x = asignacion[h].anadirGrupo(idGrupo, s, i, prob);
        calculaTiempoTotal();
        calculaTiempoPrioritario();
        return x;

    }

    /**
     * Implementación del método toString de Object
     * @return String que representa el estado
     */
    @Override
    public String toString() {
        String s = new String();
        for (int i = 0; i < asignacion.length; ++i) {
            s = s + "salidas del helicoptero " + i + "(" + asignacion[i].size() + ")\n";
            for (int j = 0; j < asignacion[i].size(); ++j) {
                try {
                    s = s + asignacion[i].getSalida(j).printGrupo(prob.GetGrupos());
                } catch (Exception e) {
                    System.out.println(e.getStackTrace());
                    System.out.println(e.getMessage());
                }
            }
            s = s + "tiempo total: " + asignacion[i].getTiempoTotal() + "\n\n";
        }
        return s;
    }

    /**
     * Elimina una salida sin grupos asignados
     * @param h Índice del helicóptero que tiene la salida vacía
     * @param s Índice de una salida vacía del helicóptero h
     * @return true si se ha eliminado la salida, false en caso contrario
     */
    public boolean eliminarSalidaVacia(int h, int s) {
        boolean b = asignacion[h].eliminarSalidaVacia(s);

        return b;
    }

}
