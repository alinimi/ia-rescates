/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estat;

import IA.Desastres.Grupos;
import java.util.LinkedList;
import util.Coordenadas;
import util.Salida;

/**
 *
 * @author Alicia
 */
public class Helicoptero {

    /**
     * LinkedList de salidas
     */
    private LinkedList<Salida> gruposRescatados;
    /**
     * Coordenadas del centro al que pertenece el helicóptero
     */
    private Coordenadas centro;
    /**
     * Tiempo en minutos que tarda el helicóptero en llevar al centro al último grupo con
     * heridos que tiene asignado
     */
    private int tiempoHeridos;
    /**
     * Tiempo que tarda el helicóptero en recoger a todos sus grupos
     */
    private int tiempoTotal;

    /**
     * Constructor de helicóptero
     * @param x Componente x de las coordenadas del centro al que pe
     * @param y
     */
    public Helicoptero(int x, int y) {
        centro = new Coordenadas(x, y);
        gruposRescatados = new LinkedList<>();
        tiempoHeridos = 0;
        tiempoTotal = 0;
    }

    /**
     * Constructor copia
     * @param h Una instancia de Helicoptero
     */
    public Helicoptero(Helicoptero h) {
        this.centro = new Coordenadas(h.centro);
        this.tiempoHeridos = h.tiempoHeridos;
        this.tiempoTotal = h.tiempoTotal;
        this.gruposRescatados = new LinkedList();
        for (Salida s : h.gruposRescatados) {
            this.gruposRescatados.add(new Salida(s));
        }
    }

    /**
     * Retorna el número de salidas que hace el helicóptero
     * @return Número de salidas que hace el helicóptero
     */
    public int size() {
        return gruposRescatados.size();
    }

    /**
     * Añade un grupo a la última salida que hace el helicóptero o a una nueva
     * salida si no cabe.
     * @param idGrupo id del grupo que se quiere añadir
     * @param g Instancia del problema
     */
    public void anadirAlFinal(int idGrupo, AI_Desastres g) {
        Grupos grup = g.GetGrupos();
        if (gruposRescatados.size() > 0
                && gruposRescatados.getLast().adicionarGrupo(idGrupo, gruposRescatados.getLast().getNumGrupos() + 1, grup)) {
            calcularTiempoSalida(gruposRescatados.getLast(), grup);
        } else {
            Salida newS = new Salida();
            gruposRescatados.addLast(newS);
            newS.adicionarGrupo(idGrupo, 0, grup);
            calcularTiempoSalida(newS, grup);
        }

    }

    /**
     * Añade un grupo a la última salida que hace el helicóptero si esta salida
     * tiene menos de dos grupos, o a una nueva salida si la última tiene más
     * de dos grupos o el nuevo grupo no cabe.
     * @param idGrupo id del grupo que se quiere añadir
     * @param g Instancia del problema
     */
    public void anadirMaxDos(int idGrupo, AI_Desastres g) {
        Grupos grup = g.GetGrupos();
        Salida s = null;
        if (gruposRescatados.size() > 0) {
            s = gruposRescatados.getLast();
        }
        if (gruposRescatados.size() > 0 && (s.getNumGrupos() < 2 && s.adicionarGrupo(idGrupo, s.getNumGrupos() + 1, grup))) {
            calcularTiempoSalida(s, grup);
        } else {
            Salida newS = new Salida();
            gruposRescatados.addLast(newS);
            newS.adicionarGrupo(idGrupo, 0, grup);
            calcularTiempoSalida(newS, grup);

        }
    }

    /**
     * Añade un grupo en una posición dada del rescate
     * @param idGrupo id del grupo que se quiere añadir
     * @param sIndex Índice de la salida del helicóptero donde se quiere añadir
     * el grupo
     * @param i Orden de la salida sIndex donde se quiere añadir el grupo
     * @param g Instancia del problema
     * @return true si se ha añadido el grupo, false en caso contrario
     */
    public boolean anadirGrupo(int idGrupo, int sIndex, int i, AI_Desastres g) {
        Salida s = gruposRescatados.get(sIndex);
        Grupos grup = g.GetGrupos();
        if (s.adicionarGrupo(idGrupo, i, grup)) {
            calcularTiempoSalida(s, grup);
            return true;
        }
        return false;

    }

    /**
     * Elimina un grupo de la asignación del helicóptero
     * @param sIndex Índice de la salida de la que se quiere eliminar un grupo
     * @param i Orden de la salida sIndex del grupo que se quiere eliminar. Tiene
     * que ser menor que el número de grupos de la salida
     * @param g instancia del problema
     * @return 
     */
    public Integer eliminarGrupo(int sIndex, int i, AI_Desastres g) {
        try {
            Salida s = gruposRescatados.get(sIndex);
            Grupos grup = g.GetGrupos();
            Integer x;
            if ((x = s.eliminarGrupo(i,g.GetGrupos())) != null) {
                if (s.getNumGrupos() == 0) {
                    gruposRescatados.remove(s);
                } else {
                    calcularTiempoSalida(s, grup);
                }
                actualizarTiempo();
                return x;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Elimina un grupo de la asignación del helicóptero
     * @param sIndex Índice de la salida de la que se quiere eliminar un grupo
     * @param i Orden de la salida sIndex del grupo que se quiere eliminar. Tiene
     * que ser menor que el número de grupos de la salida
     * @param g instancia del problema
     * @return 
     */
    public Integer eliminarGrupoTemp(int sIndex, int i, AI_Desastres g) {
        Salida s = gruposRescatados.get(sIndex);
        Grupos grup = g.GetGrupos();
        Integer x;
        if ((x = s.eliminarGrupo(i,g.GetGrupos())) != null) {
            if (s.getNumGrupos() == 0) {
                s.setTiempo(0);
            } else {
                calcularTiempoSalida(s, grup);
                actualizarTiempo();
            }
            return x;
        }
        return null;
    }

    /**
     * Elimina la salida sIndex del helicóptero si ésta está vacía
     * @param sIndex Índice de la salida vacía
     * @return true si se ha eliminado la salida, false en caso contrario
     */
    public boolean eliminarSalidaVacia(int sIndex) {
        if (gruposRescatados.get(sIndex).getNumGrupos() == 0) {
            gruposRescatados.remove(sIndex);
            actualizarTiempo();
            return true;
        }
        return false;

    }

    /**
     * Cambia el orden de las salidas i1 e i2.
     * @param i1 Índice de una salida del helicóptero
     * @param i2 Índice de una salida del helicóptero
     * @param g Instancia del problema
     * @return true si se ha hecho el cambio, false si el primer grupo no existe
     * o el segundo indice es demasiado grande
     */
    public boolean cambiarOrden(int i1, int i2, AI_Desastres g) {
        Salida s1 = gruposRescatados.get(i1);
        if (s1 == null) {
            return false;
        }
        Salida s2 = gruposRescatados.get(i2);
        if (s2 == null) {
            return false;
        }
        gruposRescatados.set(i2, s1);
        gruposRescatados.set(i1, s2);
        actualizarTiempo();
        return true;
    }

    private void calcularTiempoSalida(Salida s, Grupos grup) {
        Coordenadas cAnt;
        cAnt = centro;
        Coordenadas c = null;
        int tMin = 0;
        for (int i = 0; i < s.getNumGrupos(); ++i) {
            int nGrup = s.getGruposRecogidos()[i];

            c = new Coordenadas(grup.get(nGrup).getCoordX(), grup.get(nGrup).getCoordY());
            tMin += cAnt.distancia(c) * 60 / (100);
            if (grup.get(nGrup).getPrioridad() == 1) {
                tMin += (double) grup.get(nGrup).getNPersonas() * 2;
            } else {
                tMin += (double) grup.get(nGrup).getNPersonas();
            }
            cAnt = c;

        }
        tMin += c.distancia(centro) * 60 / (100);
        s.setTiempo(tMin);
        actualizarTiempo();

    }

    /**
     * Actualiza el tiempo del helicóptero y el de la salida pasada como
     * parámetro,
     *
     */
    public void actualizarTiempo() {
        if(gruposRescatados.size() == 0){
            tiempoHeridos = tiempoTotal = 0;
        }
        else{
            tiempoHeridos = tiempoTotal = 0;
            int lastP = 0;
            for (int i = 0; i < gruposRescatados.size(); ++i) {
                //System.out.println("tiempot: "+tiempoTotal);
                Salida s = gruposRescatados.get(i);
                tiempoTotal += s.getTiempo();
                if (s.getNumGruposPrioridad() > 0) {
                    lastP = i;
                    tiempoHeridos += s.getTiempo();
                    //System.out.println("tiempoh: "+tiempoHeridos);
                }
            }
            tiempoHeridos += lastP * 10;
            tiempoTotal += (gruposRescatados.size() - 1) * 10;
        }
    }

    public Coordenadas getCoordenadasCentro() {
        return centro;
    }

    public int getTiempoHeridos() {
        return tiempoHeridos;
    }

    public int getTiempoTotal() {
        return tiempoTotal;
    }

    public int getidGrupo(int s, int i) {
        return gruposRescatados.get(s).getidGrupo(i);
    }

    public Salida getSalida(int i) {
        return gruposRescatados.get(i);
    }

    public boolean sustituirGrupo(int id, int salida, int indice, AI_Desastres prob) {
        boolean b = gruposRescatados.get(salida).sustituirGrupo(id, indice, prob.GetGrupos());
        calcularTiempoSalida(gruposRescatados.get(salida), prob.GetGrupos());
        return b;
    }

}
