/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package estat;

import aima.search.framework.GoalTest;

/**
 *
 * @author Alicia
 */
public class RescatesGoalTest implements GoalTest {
    public RescatesGoalTest(){
    }

    /**
     * Comprueba si el EstadoRescates pasado como parámetro es un estado óptimo
     * @param o Instancia de EstadoRescates
     * @return false en todos los casos, porque el coste de comprobar si
     * un estado es un estado objetivo no es admisible
     */
    @Override
    public boolean isGoalState(Object o) {
        return false;
    }
}
