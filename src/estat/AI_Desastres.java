/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estat;
import IA.Desastres.Centro;
import IA.Desastres.Centros;
import IA.Desastres.Grupo;
import IA.Desastres.Grupos;
import java.util.Random;

/**
 *
 * @author Felina
 */
public class AI_Desastres {


    /**
     * Conjunto de grupos que hay que rescatar
     */
    private Grupos grupos;
    /**
     * Conjunto de centros de rescate del problema
     */
    private Centros centros;
    /**
     * Seed usada para generar aleatoriamente centros y grupos
     */
    private int seed;
    /**
     * Número de grupos que hay que rescatar
     */
    private final int nGrupos;
    /**
     * Número de centros de rescate en el mapa
     */
    private final int nCentros;
    /**
     * Número de helicópteros que tiene cada centro de rescate
     */
    private final int nHelicopteros;
     
    /**
     * Constructor que genera un problema con un número aleatorio de Centros,
     * Grupos y Helicópteros por centro
     * @param seed 
     */
    public AI_Desastres(int seed){
    this.seed = seed;
    Random rand = new Random();
    this.nGrupos = rand.nextInt(120)+20;
    this.grupos = new Grupos(nGrupos,seed);
    this.nCentros = rand.nextInt(15)+5;
    this.nHelicopteros = rand.nextInt(3)+1;
    this.centros = new Centros(nCentros, nHelicopteros, seed);
   }
    
    public AI_Desastres(int nGrupos, int nCentros, int nHelicopteros){
    Random rand = new Random();
    this.grupos = new Grupos(nGrupos,rand.nextInt(9999));
    this.centros = new Centros(nCentros, nHelicopteros, rand.nextInt(9999)); 
    this.nCentros = nCentros;
    this.nGrupos = nGrupos;
    this.nHelicopteros = nHelicopteros;
    }
    
    /**
     * Constructor que genera un problema de un tamaño específico, utilizando
     * una seed dada.
     * @param nGrupos Número de grupos del problema
     * @param nCentros Número de centros en el mapa
     * @param nHelicopteros Número de helicópteros en cada centro
     * @param seed Seed para la generación aleatoria de Centros y Grupos
     */
    public AI_Desastres(int nGrupos, int nCentros, int nHelicopteros, int seed){
    this.seed = seed;
    this.nCentros = nCentros;
    this.nGrupos = nGrupos;
    this.nHelicopteros = nHelicopteros;
    this.grupos = new Grupos(nGrupos,seed);
    this.centros = new Centros(nCentros, nHelicopteros, seed);  
    }
    
    public static void main(String[] args) {    
      
        //tests 
        /*
        AI_Desastres destr = new AI_Desastres(54);
        AI_Desastres destr2 = new AI_Desastres(20,5, 2, 44);
        System.out.println(centro_toString(destr.getCentros().get(0)));
        System.out.println(centros_toString(destr2.getCentros())); //*/

    }
    
    public static String grupo_toString(Grupo grupo)
    {
        return "X: "+grupo.getCoordX()+" Y: "+grupo.getCoordY()+" Pr: "+grupo.getPrioridad()+" Pers: "+grupo.getNPersonas();
    }
    
    //TODO
    public static String grupos_toString(Grupos grupos)
    {
        String groupString = "Grupos: \n";
        int i =  0;
        try{
        while(i < grupos.size())
        {
        groupString = groupString + grupo_toString(grupos.get(i)) + "\n";
        i++;
        }
        }
        catch(Exception e){
            //System.err.println(e);
        }
        return(groupString);
    }
    
    public static String centro_toString(Centro centro){    
        return("X: "+centro.getCoordX()+" Y: "+centro.getCoordY()+" Helicopteros: "+centro.getNHelicopteros());
    }
    
    public static String centros_toString(Centros centros)
    {
        String centrosString = "Centros: \n";
        int i = 0;
        int j = 0;
        try{
        while(i <centros.size())
        {
        centrosString = centrosString + "Centro "+ j +": "+ centro_toString(centros.get(i)) + "\n";
        i++;
        j++;
        }
        }
        catch(Exception e){
            System.err.println(e);
        }
        return(centrosString);
    }
    
    /**
     * getter de Grupos
     * @return Grupos
     */
    public Grupos GetGrupos(){
        return(this.grupos);
    }
    
    /**
     * getter de Centros
     * @return Centros
     */
    public Centros getCentros(){
        return(this.centros);
        
    }
    
    /**
     * getter del seed
     * @return seed
     */
    public int getSeed(){
        return(this.seed);
    }
    
    /**
     * getter del número de helicópteros por centro
     * @return nHelicopteros
     */
    public int getNHelicopteros(){
        return nHelicopteros;
    }
    
    /**
     * getter del número de centros
     * @return nCentros
     */
    public int getNCentros(){
        return nCentros;
    }
    
    /**
     * getter del número de grupos
     * @return nGrupos
     */
    public int getNGrupos(){
        return nGrupos;
    }
}
