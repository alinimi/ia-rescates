/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estat;

import aima.search.framework.Successor;
import aima.search.framework.SuccessorFunction;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Alicia
 */
public class RescatesSuccessorFunction implements SuccessorFunction {

    public RescatesSuccessorFunction() {
    }

    @Override
    public List getSuccessors(Object o) {
        HeuristicFunctionTotalTime heu = new HeuristicFunctionTotalTime();
        EstadoRescates estat = (EstadoRescates) o;
        List successors = new ArrayList();
        /*for(int i = 0; i < estat.getNumH(); ++i){
         for(int j = 0; j < estat.getNumSalidas(i); ++j){
         for(int k = j; k < estat.getNumSalidas(i); ++k){
         if(j!= k){
         System.out.println("primer tipo de sucesores");
         EstadoRescates e = new EstadoRescates(estat);
         e.cambiarOrdenRescate(i, j, k);
         e.printOrden();
         System.out.println("valor del heuristico "+heu.getHeuristicValue(e));
         successors.add(new Successor("cambiar salida "+j+
         " por "+k+"en el helicoptero"+i,e));
         }
         }
         }
         }*/
        //i es el num de heli
        //System.out.println("");
        /*
         for(int i = 0; i< estat.getNumH();++i){
         //j es el numero de la salida
         for(int j = 0; j < estat.getNumSalidas(i);++j){
         //k es la posicion
         for(int k = 0; k < estat.getSalida(i,j).getNumGrupos(); ++k){
         for(int l = i; l < estat.getNumH(); ++l){
         for(int m = j; m< estat.getNumSalidas(l);++m){
         for(int n = k; n < estat.getSalida(l, m).getNumGrupos();++n){
         if(i!=l || j!=m || k!=n){
         EstadoRescates e = new EstadoRescates(estat);
         e.intercambiarGrupo(i, j, k, l, m, n);
         successors.add(new Successor("cambiar el grupo "+
         k+" de la salida "+j+" del helicoptero "+
         i+" por el grupo "+n+" de la salida "+
         m+" del helicoptero "+l,e));
         }
         }
         }
         }
         }
         }
         }
         */
        //System.out.println("");

        for (int i = 0; i < estat.getNumH(); ++i) {
            //j es el numero de la salida
            for (int j = 0; j < estat.getNumSalidas(i); ++j) {
                //k es la posicion
                for (int k = 0; k < estat.getSalida(i, j).getNumGrupos(); ++k) {
                    for (int l = 0; l < estat.getNumH(); ++l) {
                        for (int m = 0; m < estat.getNumSalidas(l); ++m) {
                            for(int n = 0; n < estat.getSalida(l,m).getNumGrupos();++n){
                            //int n = estat.getSalida(l, m).getNumGrupos();
                                if (i != l || j != m) {
                                    EstadoRescates e = new EstadoRescates(estat);
                                    boolean b = false;
                                    b = e.cambiarUnGrupo(i, j, k, l, m, n);
                                    if (b) {
                                        successors.add(new Successor("cambiar el grupo "
                                                + k + " de la salida " + j + " del helicoptero "
                                                + i + " por el grupo " + n + " de la salida "
                                                + m + " del helicoptero " + l, e));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return successors;
    }

}
